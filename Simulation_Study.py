import numpy as np
import math
import pandas as pd
from Simulate_Data import Simulate_Data
from Cox_Regression import Cox_Regression


# Python code for simulation study to compare machine learning and statistical methods - SAfJR

# Function to simulate a single dataset
def simulate_data(n, params, coeff, baseline):
    # Simulate binary variable
    i = 0
    x = []
    while i < n:
        ind = np.random.choice([0.0, 1.0], p=[0.5, 0.5])
        x.append(ind)
        i += 1

    # Make an array of coeff values - so numpy can multiply
    coeff = np.full(n, coeff)

    # Simulate survival times according to baseline hazard
    s = Simulate_Data(n)
    times = s.simulate_times(x, coeff, params[0], params[1], dist=baseline)

    # Make an event indicator variable, applying admin censoring at t = 10
    event = (times < 10).astype(int)
    times[times > 10] = 10

    df = {'Time': times,
          'Event': event,
          'Covariate': x}

    df = pd.DataFrame(df)

    return df


# Function to generate m datasets with n observations, params, coeff, baseline
def generate_datasets(m, n, params, coeff, baseline):
    # Set the seed for numpy.random
    np.random.seed(0)

    # Generate m training datasets
    train_data_sets = []
    i = 0
    while i < m:
        df = simulate_data(n, params, coeff, baseline)
        train_data_sets.append(df)
        i += 1

    # Generate m test datasets
    test_data_sets = []
    i = 0
    while i < m:
        df = simulate_data(n, params, coeff, baseline)
        test_data_sets.append(df)
        i += 1


    # Return an array of train dataframes and an array of train datasets
    return train_data_sets, test_data_sets


def fit_model(train_data_sets, model):
    # Fit model to each of the datasets
    estimates = []
    for df in train_data_sets:
        # Fit Cox model
        if model == 'Cox':
            cox = Cox_Regression(df)
            b = cox.cox_regression()

        # Fit flexible parametric model
        if model == 'FPM':
            b = 0

        # Fit neural network model

        # Fit RSF model
        if model == 'RSF':
            b = 0

        estimates.append(b)

    return estimates


# Fit a model to each data set for each model type specified in models array
def fit_all_models(train_data_sets, test_data_sets, models):
    for m in models:
        estimates = fit_model(train_data_sets, m)
        # Get the average estimate using the test datasets

    # Return an array of each average for each model type?

    return estimates

'''
print('Working')
train_data_sets, test_data_sets = generate_datasets(1, 1000, [0.2, 1], -0.5, 'exponential')
print('Data generated')
estimates = fit_model(train_data_sets, 'Cox')
print(estimates)
'''


