import numpy as np
import math
import scipy.optimize
import lifelines
from matplotlib import pyplot as plt



class Cox_Regression:
    """
    Class for fitting a Cox PH model to estimate the hazard rate

    Attributes:
        events: Array of the events observed
        times: Array of the times the events were observed
        sample_size: Int referring to how many observations are in the dataset
        x: 2D array of covariate values for each observation
    """

    def __init__(self, df):
        # Initiate the attributes and create table of survival function estimates
        self.df = df
        self.events = self.df['Event']
        self.times = self.df['Time']
        self.sample_size = len(self.times)
        self.x = self.df['Covariate']

    def loss_function(self, b):
        # SUM(D_i * log(SUM(exp(xj - xi)))
        b = b[0]
        loss = 0
        for i in range(len(self.times)):
            d = self.events[i]
            xj = []
            for j in range(len(self.times)):
                if self.times[j] >= self.times[i] and j != i:
                    xj.append(self.x[j])
            xi = np.full(len(xj), self.x[i])
            sum = np.sum(np.exp(b * (xj - xi)))
            loss = loss + (d * math.log(sum))

        return loss

    def cox_regression(self):
        cph = lifelines.CoxPHFitter().fit(self.df, 'Time', 'Event')
        b = cph.params_

        cph.predict_survival_function(self.df)
        cph.plot_covariate_groups('Covariate', [0, 1], cmap='coolwarm')
        plt.show()

        return b