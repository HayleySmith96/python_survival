import scipy.optimize
import numpy as np
import math
from Simulate_Data import Simulate_Data
from lifelines import WeibullFitter, ExponentialFitter


# Log-likelihood for EXPONENTIAL
def exponential_LL(theta, t, d):
    # db - exp(b)*t
    b = theta[0]
    log_likelihood = d * b - math.exp(b) * t
    log_likelihood = np.sum(log_likelihood)

    return -1 * log_likelihood


# Test SciPy for EXPONENTIAL
simulate = Simulate_Data(10000)
lam = 0.2
gamma = 1
t = simulate.simulate_times(0, 0, lam, gamma, dist='exponential')
d = np.full(10000, 1)

theta = np.array([1])
theta = np.log(theta)

res = scipy.optimize.minimize(exponential_LL, theta, args=(t, d))
print(res)
est = res.get('x')
print("SciPy Estimate = ", np.exp(est))

# Test Lifelines
exf = ExponentialFitter().fit(t, d, label='ExponentalFitter')
exf.print_summary()


# Log-likelihood for WEIBULL
def weibull_LL(theta, t, d):
    # d[b1 + b2 + (exp(b2) - 1)*log(t)] - exp(b1)t^exp(b2)
    b1 = theta[0]
    b2 = theta[1]
    log_likelihood = b1 + b2 + (math.exp(b2) - 1) * np.log(t)
    log_likelihood = np.multiply(log_likelihood, d) - (math.exp(b1) * np.power(t, math.exp(b2)))
    log_likelihood = np.sum(log_likelihood)

    return -1 * log_likelihood


# Test SciPy for WEIBULL
lam = 0.2
gamma = 2
t = simulate.simulate_times(lam, gamma, dist='weibull')

theta = np.array([1, 1])
theta = np.log(theta)

res = scipy.optimize.minimize(weibull_LL, theta, args=(t, d))
print(res)
est = res.get('x')
print("SciPy Estimate = ", np.exp(est))

# Test Lifelines
wbf = WeibullFitter().fit(t, d, label='WeibullFitter')
wbf.plot_survival_function()


# Log-likelihood for GOMPERTZ
def gompertz_LL(theta, t, d):
    # B1 + exp(B2)dt - exp(B1 - B2)*(exp(t * exp(B2)) - 1)
    b1 = theta[0]
    b2 = theta[1]
    log_likelihood = b1 + math.exp(b2) * np.multiply(d, t) - (math.exp(b1 - b2) * (np.exp(t * math.exp(b2)) - 1))
    log_likelihood = np.sum(log_likelihood)

    return -1 * log_likelihood


# Test SciPy for GOMPERTZ
lam = 0.2
gamma = 2
t = simulate.simulate_times(lam, gamma, dist='gompertz')

theta = np.array([1, 1])
theta = np.log(theta)

res = scipy.optimize.minimize(gompertz_LL, theta, args=(t, d))
print(res)
est = res.get('x')
print("SciPy Estimate = ", np.exp(est))
