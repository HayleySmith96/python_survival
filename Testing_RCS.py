# File to test the RCS code - python script
from Simulate_Data import Simulate_Data
from Kaplan_Meier import Kaplan_Meier
from Parametric_Models import Parametric_Models
from Restricted_Cubic_Splines import RCS
import numpy as np
import math


##########---------- Simulate Data ----------##########

# Simulated data object
n = 10000   # Number of observations
S = Simulate_Data(n)

# Simulate binary variables
p = 0.5  # Probability the variable is 1
covar_1 = S.simulate_binary_var(p)

# Simulate a continuous variable
covar_cont = S.simulate_cont_var(60, 10, dist='normal')

# Array of covariate arrays
covar = np.c_[covar_1, covar_cont]

# Simulate times
coeff = [-0.5, 0.1]
data = S.simulate_times(covar, coeff, 0.2, gamma=1, dist='weibull', max_time=10)


##########---------- Fit Kaplan-Meier ----------##########

# Create Kaplan-Meier object
#km = Kaplan_Meier(data, CI=True)
#kaplan_meier_estimates = km.fit_kaplan_meier()
#print(kaplan_meier_estimates)


##########---------- Fit Model ----------##########

# Create model object
fit = Parametric_Models()

# Create initial parameters, maximum iterations and convergence
theta = [1]
max_iter = 10000
convergence = 0.001

# Fit an exp model to the data
parameters = fit.fit_model(data, model='exponential', init_parameters=theta, max_iter=max_iter, convergence=convergence)

# Predict S(t), h(t) and H(t)
predict_times = [0, 1, 2]
predictions = fit.predict(predict_times)
print(predictions)

'''
##########---------- Fit RCS ----------##########
data['Time'] = np.log(data['Time'])
rcs = RCS(data, df=1)
estimates = rcs.fit_RCS()

answer = estimates.copy()  # Copy of the estimates array so doesn't exponent the rcs attribute
answer[0] = math.exp(answer[0])
print('Parameter estimates: ', answer)

# Survival prediction
times = [0, 2, 4, 6, 8, 10]
covariates = [1, 60]

predictions_RCS = rcs.predict_RCS(times, baseline=False, covariates=covariates)
print(predictions_RCS)
'''
